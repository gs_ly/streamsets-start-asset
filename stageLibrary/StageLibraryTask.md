# StageLibraryTask
## 类图

![StageLibraryTask](image/StageLibraryTask.png)

1. Task: 定义了任务的基本方法（包括启动、停止等生命周期方法）
2. ClassLoaderReleaser: 只有一个方法releaseStageClassLoader，用来释放类加载器



## 子类

### ClassLoaderStageLibraryTask
ps:在streamset启动过程中主要逻辑就是执行了该类的initTask方法，因为该子任务并未实现runTask方法

#### 主要方法

initTask：仅介绍部分重要代码


    `public void initTask() {
         ......
         // 从libraries中加载所有stage和其它对象
         json = ObjectMapperFactory.get();
         stageLibraries = new ArrayList<>();
         stageLibraryMap = new HashMap<>();
         stageList = new ArrayList<>();
         stageMap = new HashMap<>();
         // 重点！！！加载过程，下面介绍
         loadStages();
         ......
         // 初始化私有的类加载器池
         GenericKeyedObjectPoolConfig poolConfig = new GenericKeyedObjectPoolConfig();
         poolConfig.setJmxEnabled(false);
         int maxPrivateClassloaders = configuration.get(MAX_PRIVATE_STAGE_CLASS_LOADERS_KEY, MAX_PRIVATE_STAGE_CLASS_LOADERS_DEFAULT);
         poolConfig.setMaxTotal(maxPrivateClassloaders);
         poolConfig.setMinEvictableIdleTimeMillis(-1);
         poolConfig.setNumTestsPerEvictionRun(0);
         poolConfig.setMaxIdlePerKey(-1);
         poolConfig.setMinIdlePerKey(0);
         poolConfig.setMaxTotalPerKey(-1);
         poolConfig.setBlockWhenExhausted(false);
         poolConfig.setMaxWaitMillis(0);
         privateClassLoaderPool = new GenericKeyedObjectPool<>(new ClassLoaderFactory(stageClassLoaders), poolConfig);
     
         // 监控私有类加载器池的使用情况
         this.gaugeMap = MetricsConfigurator.createFrameworkGauge(
           runtimeInfo.getMetrics(),
           "classloader.private",
           "runtime",
           null
         ).getValue();
         this.gaugeMap.put(PRIVATE_POOL_ACTIVE, new AtomicInteger(0));
         this.gaugeMap.put(PRIVATE_POOL_IDLE, new AtomicInteger(0));
         this.gaugeMap.put(PRIVATE_POOL_MAX, maxPrivateClassloaders);
         //如果不是处于streamset云环境，则捞取官网仓库中的 stage library定义信息
         //请注意，该段代码功能可能需要翻墙
         if (!Boolean.getBoolean("streamsets.cloud")) {
           // auto load stage library definitions
           Thread thread = new Thread(this::getRepositoryManifestList);
           thread.setDaemon(true);
           thread.setName("ManifestFetcher");
           thread.start();
     }`

loadStages：仅介绍部分重要代码


    `void loadStages() {
         String javaVersion = System.getProperty("java.version");
         Version sdcVersion = new Version(buildInfo.getVersion());
         ......
         
         try {
           int libs = 0;
           int stages = 0;
           int lineagePublishers = 0;
           int credentialStores = 0;
           int services = 0;
           int interceptors = 0;
           int delegates = 0;
           long start = System.currentTimeMillis();
           LocaleInContext.set(Locale.getDefault());
           for (ClassLoader cl : stageClassLoaders) {
             try {
               //验证stage于jvm版本兼容性，sdc版本要求
               ...... 

           // 获取stages-lib定义对象
           StageLibraryDefinition libDef = StageLibraryDefinitionExtractor.get().extract(cl);
           libDef.setVersion(getPropertyFromLibraryProperties(cl, "version", ""));
           LOG.debug("Loading stages and plugins from library '{}' on version {}", libDef.getName(), libDef.getVersion());
           stageLibraries.add(libDef);
           stageLibraryMap.put(libDef.getName(), libDef);
           libs++;
 
           // Load Stages,底层通过JAVA-APT机制在编译时将需要加载的类写入PipelineStages.json文件，
           // 然后在此时直接取出进行然后生成StageDefinition实例
           for(Class klass : loadClassesFromResource(libDef, cl, STAGES_DEFINITION_RESOURCE)) {
             stages++;
             StageDefinition stage = StageDefinitionExtractor.get().extract(libDef, klass, Utils.formatL("Library='{}'", libDef.getName()));
             String key = createKey(libDef.getName(), stage.getName());
             LOG.debug("Loaded stage '{}'  version {}", key, stage.getVersion());
             stageList.add(stage);
             stageMap.put(key, stage);
 
             for(Class eventDefClass : stage.getEventDefs()) {
               if (!eventDefinitionMap.containsKey(eventDefClass.getCanonicalName())) {
                 eventDefinitionMap.put(
                     eventDefClass.getCanonicalName(),
                     EventDefinitionExtractor.get().extractEventDefinition(eventDefClass)
                 );
               }
             }
           }
 
           // Load Lineage publishers
           // 底层通过JAVA-APT机制在编译时将需要加载的类写入LineagePublishers.json文件，
           // 然后在此时直接取出进行然后生成LineagePublisherDefinition实例
           for(Class klass : loadClassesFromResource(libDef, cl, LINEAGE_PUBLISHERS_DEFINITION_RESOURCE)) {
             lineagePublishers++;
             LineagePublisherDefinition lineage = LineagePublisherDefinitionExtractor.get().extract(libDef, klass);
             String key = createKey(libDef.getName(), lineage.getName());
             LOG.debug("Loaded lineage plugin '{}'", key);
             lineagePublisherDefinitions.add(lineage);
             lineagePublisherDefinitionMap.put(key, lineage);
           }
 
           // Load Credential stores
           // 底层通过JAVA-APT机制在编译时将需要加载的类写入CredentialStores.json文件，
           // 然后在此时直接取出进行然后生成CredentialStoreDefinition实例     
           for(Class klass : loadClassesFromResource(libDef, cl, CREDENTIAL_STORE_DEFINITION_RESOURCE)) {
             credentialStores++;
             CredentialStoreDefinition def = CredentialStoreDefinitionExtractor.get().extract(libDef, klass);
             String key = createKey(libDef.getName(), def.getName());
             LOG.debug("Loaded credential store '{}'", key);
             credentialStoreDefinitions.add(def);
           }
 
           // Load Services
           // 底层通过JAVA-APT机制在编译时将需要加载的类写入Services.json文件，
           // 然后在此时直接取出进行然后生成ServiceDefinition实例     
           for(Class klass : loadClassesFromResource(libDef, cl, SERVICE_DEFINITION_RESOURCE)) {
             services++;
             ServiceDefinition def = ServiceDefinitionExtractor.get().extract(libDef, klass);
             LOG.debug("Loaded service for '{}'", def.getProvides().getCanonicalName());
             serviceList.add(def);
             serviceMap.put(def.getProvides(), def);
           }
 
           // Load Interceptors
           // 底层通过JAVA-APT机制在编译时将需要加载的类写入Interceptors.json文件，
           // 然后在此时直接取出进行然后生成InterceptorDefinition实例
           for(Class klass : loadClassesFromResource(libDef, cl, INTERCEPTOR_DEFINITION_RESOURCE)) {
             interceptors++;
             InterceptorDefinition def = InterceptorDefinitionExtractor.get().extract(libDef, klass);
             LOG.debug("Loaded interceptor '{}'", def.getKlass().getCanonicalName());
             interceptorList.add(def);
           }
 
           // Load Delegates
           // 底层通过JAVA-APT机制在编译时将需要加载的类写入Delegates.json文件，
           // 然后在此时直接取出进行然后生成StageLibraryDelegateDefinitition实例
           for(Class klass : loadClassesFromResource(libDef, cl, DELEGATE_DEFINITION_RESOURCE)) {
             delegates++;
             StageLibraryDelegateDefinitition def = StageLibraryDelegateDefinitionExtractor.get().extract(libDef, klass);
             String key = createKey(libDef.getName(), def.getExportedInterface().getCanonicalName());
             LOG.debug("Loaded delegate '{}'", def.getKlass().getCanonicalName());
             delegateList.add(def);
             delegateMap.put(key, def);
           }
         } catch (IOException | ClassNotFoundException ex) {
           throw new RuntimeException(
               Utils.format("Could not load stages definition from '{}', {}", cl, ex.toString()), ex);
         }
       }
       LOG.info(
         "Loaded '{}' libraries with a total of '{}' stages, '{}' lineage publishers, '{}' services, '{}' interceptors, '{}' delegates and '{}' credentialStores in '{}ms'",
         libs,
         stages,
         lineagePublishers,
         services,
         interceptors,
         delegates,
         credentialStores,
         System.currentTimeMillis() - start
       );
     } finally {
       LocaleInContext.set(null);
     }


### PreviewStageLibraryTask
预览相关-待补充

### MockStageLibraryTask

mock使用-待补充
